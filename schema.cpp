#include "schema.h"

#include <QGraphicsScene>
#include <QGraphicsPathItem>

Schema::Schema(QGraphicsScene* scene, int cardWidth, int cardHeight, int boxDepth, bool isTop, int dpi)
    :pScene(scene), pBorder(nullptr), pTexture(nullptr),
      cardWidth(cardWidth),
      cardHeight(cardHeight),
      boxDepth(boxDepth),
      latch(5),
      doublePaperDepth(0.8),
      isTop(isTop),
      dpi(dpi)
{
    rebuildVerteces();
    externalBorder();
    internalLines();
}

QGraphicsPathItem* Schema::externalBorder()
{
    if (pBorder)
        return pBorder;

    QPen outline  = externalPen();

    const double D = sizeD();

    QPainterPath path;
    path.moveTo(vertex[ 0]);
    path.lineTo(vertex[ 1]);
    path.lineTo(vertex[ 2]);
    path.lineTo(vertex[ 3]);
    path.arcTo(vertex[29].x() - D, vertex[29].y() - D, 2*D, 2*D, 270, 90);
    path.lineTo(vertex[ 5]);
    path.lineTo(vertex[ 6]);
    path.lineTo(vertex[ 7]);
    path.lineTo(vertex[ 8]);
    path.lineTo(vertex[ 9]);
    path.lineTo(vertex[10]);
    path.arcTo(vertex[30].x() - D, vertex[30].y() - D, 2*D, 2*D, 0, 90);
    path.lineTo(vertex[12]);
    path.lineTo(vertex[13]);
    path.lineTo(vertex[14]);
    path.lineTo(vertex[15]);
    path.lineTo(vertex[16]);
    path.lineTo(vertex[17]);
    path.arcTo(vertex[31].x() - D, vertex[31].y() - D, 2*D, 2*D, 90, 90);
    path.lineTo(vertex[19]);
    path.lineTo(vertex[20]);
    path.lineTo(vertex[21]);
    path.lineTo(vertex[22]);
    path.lineTo(vertex[23]);
    path.lineTo(vertex[24]);
    path.arcTo(vertex[28].x() - D, vertex[28].y() - D, 2*D, 2*D, 180, 90);
    path.lineTo(vertex[26]);
    path.lineTo(vertex[27]);
    path.lineTo(vertex[0]);
    path.closeSubpath();

    pBorder = pScene->addPath(path, outline);
    pBorder->setZValue(1);
    pBorder->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);

    return pBorder;
}

QList<QGraphicsLineItem*>& Schema::internalLines()
{
    if (!linesList.isEmpty())
        return linesList;

    QLineF line;
    QGraphicsLineItem* pItem;
    QPen pen = internalPen();

    line.setPoints(vertex[27], vertex[2]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[26], vertex[3]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[24], vertex[5]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[19], vertex[10]);
    pItem = pScene->addLine(line, internalPen());
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[17], vertex[12]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[16], vertex[13]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[20], vertex[23]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[19], vertex[24]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[17], vertex[26]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[12], vertex[3]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[10], vertex[5]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    line.setPoints(vertex[9], vertex[6]);
    pItem = pScene->addLine(line, pen);
    pItem->setParentItem(externalBorder());
    pItem->setZValue(1);
    linesList.append(pItem);

    return linesList;
}

QGraphicsPixmapItem* Schema::setTexture(const QPixmap& texture)
{
    delete pTexture;
    pTexture = addImage(Side::E, texture, Qt::AlignCenter, false);
    pTexture->parentItem()->setZValue(0);
    pTexture->setFlag(QGraphicsItem::ItemIsMovable, false);
    pTexture->setFlag(QGraphicsItem::ItemIsFocusable, false);
    pTexture->setFlag(QGraphicsItem::ItemIsSelectable, false);
    return pTexture;
}

QGraphicsPixmapItem* Schema::addImage(Schema::Side side, const QPixmap& pixmap, Qt::Alignment alignment, bool symmetric)
{
    QRectF sr = sideRect(side);
    QGraphicsRectItem* pRect = pScene->addRect(sr, Qt::NoPen);
    pRect->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
    pRect->setParentItem(externalBorder());
    pRect->setZValue(2);

    double scale_w = sr.width();
    double scale_h = sr.height();
    if (qAbs(sideOrientation(side)) == 90)
    {
        scale_h = sr.width();
        scale_w = sr.height();
    }
    QGraphicsPixmapItem* pPix = pScene->addPixmap(pixmap.scaled(scale_w, scale_h));
    pPix->setParentItem(pRect);
    pPix->setZValue(2);

    pPix->setPos(sideAnchor(side));
    pPix->setRotation(sideOrientation(side));

    pPix->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsFocusable | QGraphicsItem::ItemIsSelectable);


    if (symmetric)
    {
        Side s = correspondingSide(side);
        if (s != side)
            addImage(correspondingSide(side), pixmap, alignment, false);
    }

    return pPix;
}

QRectF Schema::frontRect() const
{
    return sideRect(Side::F);
}

QRectF Schema::sideRect(Side side) const
{
    switch (side)
    {
        case Side::F: return QRectF(vertex[31], vertex[29]);
        case Side::A: return QRectF(vertex[19], vertex[28]);
        case Side::Ai: return QRectF(vertex[20], vertex[24]);
        case Side::Al: return QRectF(vertex[21].x(), vertex[20].y(), sizeL(), sizeW());
        case Side::B: return QRectF(vertex[28], vertex[3]);
        case Side::Bi: return QRectF(vertex[26], vertex[2]);
        case Side::Bl: return QRectF(vertex[27], vertex[0]);
        case Side::a: return QRectF(vertex[30], vertex[5]);
        case Side::ai: return QRectF(vertex[10], vertex[6]);
        case Side::al: return QRect(vertex[9].x(), vertex[9].y(), sizeL(), sizeW());
        case Side::b: return QRectF(vertex[17], vertex[30]);
        case Side::bi: return QRectF(vertex[16], vertex[12]);
        case Side::bl: return QRectF(vertex[16].x(), vertex[15].y(), sizeW(), sizeL());
        case Side::E: return QRectF(vertex[18], vertex[4]);
    }
    return QRectF();
}

Schema::Side Schema::correspondingSide(Schema::Side side) const
{
    switch (side) {
        case Side::A: return Side::a;
        case Side::a: return Side::A;
        case Side::B: return Side::b;
        case Side::b: return Side::B;
        case Side::ai: return Side::Ai;
        case Side::Ai: return Side::ai;
        case Side::bi: return Side::Bi;
        case Side::Bi: return Side::bi;
        case Side::al: return Side::Al;
        case Side::Al: return Side::al;
        case Side::bl: return Side::Bl;
        case Side::Bl: return Side::bl;

        case Side::E: return Side::E;
        case Side::F: return Side::F;
    }
}

QPointF Schema::sideAnchor(Schema::Side side) const
{
    QRectF sr = sideRect(side);
    switch (side)
    {
        case Side::B:
        case Side::Bi:
        case Side::Bl:
        case Side::E:
        case Side::F:
            return sr.topLeft();

        case Side::A:
        case Side::Ai:
        case Side::Al:
            return sr.topRight();

        case Side::a:
        case Side::ai:
        case Side::al:
            return sr.bottomLeft();

        case Side::b:
        case Side::bi:
        case Side::bl:
            return sr.bottomRight();
    }

    return QPointF();
}

int Schema::sideOrientation(Schema::Side side) const
{
    switch (side)
    {
        case Side::B:
        case Side::Bi:
        case Side::Bl:
        case Side::E:
        case Side::F:
            return 0;

        case Side::A:
        case Side::Ai:
        case Side::Al:
            return 90;

        case Side::a:
        case Side::ai:
        case Side::al:
            return -90;

        case Side::b:
        case Side::bi:
        case Side::bl:
            return 180;
    }
    return 0;
}

QPen Schema::externalPen() const
{
    QPen outline(Qt::SolidLine);
    outline.setWidth(2);
    return outline;
}

QPen Schema::internalPen() const
{
    QPen dash(Qt::DashLine);
    dash.setWidth(1);
    return dash;
}

double Schema::sizeH() const
{
    double h = cardHeight + 1.5 + 2 * doublePaperDepth;
    if (isTop)
        h += 2 * doublePaperDepth + 1.5;
    return h * dpi / mm_in_inch;
}

double Schema::sizeW() const
{
    double w = cardWidth + 1.5 + 2 * doublePaperDepth;
    if (isTop)
        w += 2 * doublePaperDepth + 1.5;
    return w * dpi / mm_in_inch;
}

double Schema::sizeD() const
{
    return boxDepth * dpi / mm_in_inch;
}

double Schema::sizeL() const
{
    return 5 * dpi / mm_in_inch;
}

void Schema::rebuildVerteces()
{
    const double H = sizeH();
    const double W = sizeW();
    const double L = sizeL();
    const double D = sizeD();

    vertex.clear();

    vertex[0] = QPointF(-H / 2, D + D + W);
    vertex.insert(1, QPointF(H / 2, D + D + W)); // B -- 1
    vertex.insert(2, QPointF(H / 2, D + D ) ); // C -- 2
    vertex.insert(3, QPointF(H / 2, D ) ); // D -- 3
    vertex.insert(4, QPointF(H / 2 + D, D ) ); // E  - 4
    vertex.insert(5, QPointF(H / 2 + D, 0 ) ); //F  - 5
    vertex.insert(6, QPointF(H / 2 + D + D, 0 ) ); // G  6
    vertex.insert(7, QPointF(H / 2 + D + D + L, -L ) ); // H  7
    vertex.insert(8, QPointF(H / 2 + D + D + L, - W + L ) ); // I  8
    vertex.insert(9, QPointF(H / 2 + D + D, - W ) ); // J  9
    vertex.insert(10, QPointF(H / 2 + D, - W ) ); // K  10
    vertex.insert(11, QPointF(H / 2 + D, - W  - D) ); // L  11
    vertex.insert(12, QPointF(H / 2 , - W  - D) ); // M 12
    vertex.insert(13, QPointF(H / 2 , - W  - D - D) ); // N 13
    vertex.insert(14, QPointF(H / 2 - L, - W  - D - D - L )); // O   14
    vertex.insert(15, QPointF(-H / 2 + L, - W  - D - D - L )); // P  15
    vertex.insert(16, QPointF(-H / 2 , - W  - D - D) ); // Q   16
    vertex.insert(17, QPointF(-H / 2 , - W  - D) ); // R   17
    vertex.insert(18, QPointF(-H / 2 - D, - W  - D) ); // S  18
    vertex.insert(19, QPointF(-H / 2 - D, - W) ); // T  19
    vertex.insert(20, QPointF(-H / 2 - D - D, - W) ); // U   20
    vertex.insert(21, QPointF(-H / 2 - D - D - L, - W + L)); // V  21
    vertex.insert(22, QPointF(-H / 2 - D - D - L, - L)); // W 22
    vertex.insert(23, QPointF(-H / 2 - D - D , 0)); // X  23
    vertex.insert(24, QPointF(-H / 2 - D  , 0)); // Y  24
    vertex.insert(25, QPointF(-H / 2 - D  , D)); // Z  25
    vertex.insert(26, QPointF(-H / 2  , D)); // 1  26
    vertex.insert(27, QPointF(-H / 2  , D + D)); // 2  27}

    vertex.insert(28, QPointF(-H/2, 0)); // 28 -- 0
    vertex.insert(29, QPointF(H/2, 0)); // 29 -- 1
    vertex.insert(30, QPointF(H/2, -W)); // 30 -- 2
    vertex.insert(31, QPointF(-H/2, -W)); // 31 -- 3

}





