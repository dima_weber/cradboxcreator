#include "dialog.h"
#include "ui_dialog.h"
#include "schema.h"
#include <QGraphicsEllipseItem>
#include <QApplication>
#include <QScreen>
#include <QtPrintSupport>
#include <QFontDatabase>
#include <QPainterPath>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog),
    pScene(new QGraphicsScene),
    _dpi(72),
    pUpperPart (nullptr),
    pLowerPart (nullptr)
{
    ui->setupUi(this);
    setLayout(ui->horizontalLayout);

    ui->graphicsView->setScene(pScene);

    connect (ui->pCardHeightSpin, SIGNAL(valueChanged(double)), this, SLOT(drawSketch()));
    connect (ui->pCardWidthSpin, SIGNAL(valueChanged(double)), this, SLOT(drawSketch()));
    connect (ui->pBoxDepthSpin, SIGNAL(valueChanged(double)), this, SLOT(drawSketch()));
    connect (ui->pFrontPixmap, SIGNAL(changed(QString)), this, SLOT(onFrontImageChanged(QString)));
    connect (ui->pTexture, SIGNAL(changed(QString)), this, SLOT(onTextureChanged(QString)));
    connect (ui->pLongSidePixmap, SIGNAL(changed(QString)), this, SLOT(onLongSideImageChanged(QString)));
    connect (ui->pSideTitle, SIGNAL(textChanged(QString)), this, SLOT(drawSketch()));

    connect (ui->pPrintButton, SIGNAL(pressed()), this, SLOT(drawOnPrinter()));

    QFontDatabase::addApplicationFont(":/fonts/windlass_cyr_0.ttf");

    ui->graphicsView->scale(0.1, 0.1);

    setDpi(qApp->primaryScreen()->physicalDotsPerInch() * 10);

    drawSketch();
}

Dialog::~Dialog()
{
    delete ui;
}

double Dialog::cardWidth() const
{ return ui->pCardWidthSpin->value() * dpi() / mm_in_inch; }

double Dialog::cardHeight() const
{ return ui->pCardHeightSpin->value() * dpi() / mm_in_inch; }

double Dialog::boxDepth() const
{ return ui->pBoxDepthSpin->value() * dpi() / mm_in_inch; }

double Dialog::latchWidth() const
{ return 5 * dpi() / mm_in_inch ; }

double Dialog::dpi() const
{
    return _dpi;
}

void Dialog::setDpi(double dpi)
{
    _dpi = dpi;
}

void Dialog::drawOnPrinter()
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setPageSize(QPrinter::A4);
    printer.setFullPage(true);
    printer.setOrientation(QPrinter::Portrait);
    printer.setColorMode(QPrinter::Color);
    QPrintDialog dialog(&printer, this);
    if (dialog.exec() == QDialog::Rejected)
        return;

    QPainter painter(&printer);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setRenderHint(QPainter::TextAntialiasing);

    QRectF pageRect = printer.pageRect(QPrinter::DevicePixel);
    QRectF paperRect = printer.paperRect(QPrinter::DevicePixel);
    QRectF paperRectMm = printer.paperRect(QPrinter::Millimeter);
    int printerDpi = printer.resolution();

    for (int i=0; i<pages; i++)
    {
        QRectF sceneRect = QRectF(0, i * paperRectMm.height() / mm_in_inch * dpi(),
                                  paperRectMm.width() / mm_in_inch * dpi(), paperRectMm.height() / mm_in_inch * dpi());
        pScene->render(&painter, paperRect, sceneRect);
        if (i != pages - 1)
            printer.newPage();
    }
}

void Dialog::drawSketch()
{
    createPoints();
    pScene->clear();

    topSchema = new Schema(pScene, ui->pCardWidthSpin->value(), ui->pCardHeightSpin->value(), ui->pBoxDepthSpin->value(), true, dpi());
    bottomSchema = new Schema(pScene, ui->pCardWidthSpin->value(), ui->pCardHeightSpin->value(), ui->pBoxDepthSpin->value(), false, dpi());

    QGraphicsPathItem* pUpperPart = topSchema->externalBorder();
    QGraphicsPathItem* pLowerPart = bottomSchema->externalBorder();

    onFrontImageChanged(ui->pFrontPixmap->text());
    onTextureChanged(ui->pTexture->text());
    onLongSideImageChanged(ui->pLongSidePixmap->text());

    QFont titleFont("Windlass");
    titleFont.setPixelSize(4 * dpi() / mm_in_inch );
    QGraphicsTextItem* pTitle;
    pTitle = pScene->addText(ui->pSideTitle->text(), titleFont);
    pTitle->setRotation(90);
    pTitle->setPos(points[31]);
    pTitle->moveBy(-3 * dpi() / mm_in_inch, 3 * dpi() / mm_in_inch);
    pTitle->setParentItem(pUpperPart);
    pTitle->setZValue(4);

    pTitle = pScene->addText(ui->pSideTitle->text(), titleFont);
    pTitle->setRotation(-90);
    pTitle->setPos(points[29]);
    pTitle->moveBy(3 * dpi() / mm_in_inch, -3 * dpi() / mm_in_inch);
    pTitle->setParentItem(pUpperPart);
    pTitle->setZValue(4);
    pTitle->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsFocusable | QGraphicsItem::ItemIsSelectable);

    const double leftMargin = 4 * dpi() / mm_in_inch;
    const double rightMargin = 4 * dpi() / mm_in_inch;
    const double topMargin = 4 * dpi() / mm_in_inch;
    const double bottomMargin = 4 * dpi() / mm_in_inch;

    const double paperWidth =210 * dpi() / mm_in_inch;
    const double paperHeight = 297 * dpi() / mm_in_inch;
    const double pageWidth = paperWidth - leftMargin - rightMargin;
    const double pageHeight = paperHeight - topMargin - rightMargin;

    pages = 1;
    pUpperPart->setRotation(90);
    int x = paperWidth - rightMargin - topSchema->sizeL() - 2 * topSchema->sizeD() - topSchema->sizeW();
    int y = topMargin + topSchema->sizeL() + 2 * topSchema->sizeD() + topSchema->sizeH() / 2;
    pUpperPart->setPos(x,y);

    pLowerPart->setRotation(-90);
    x = leftMargin + bottomSchema->sizeL() + 2 * bottomSchema->sizeD() + bottomSchema->sizeW();
    y = paperHeight - bottomMargin - bottomSchema->sizeL() - 2 * bottomSchema->sizeD() - bottomSchema->sizeH()/2;
    pLowerPart->setPos(x, y);

//    if (pUpperPart->collidesWithItem(pLowerPart))
//    {
//        pUpperPart->setRotation(0);
//        x = leftMargin + latchWidth() + 2 * boxDepth() + cardHeight()/2;
//        y = topMargin + latchWidth() + 2 * boxDepth() + cardWidth();
//        pUpperPart->setPos(x, y);

//        pLowerPart->setRotation(180);
//        x = pageWidth - latchWidth() - 2 * boxDepth() - cardHeight()/2;
//        y = pageHeight - latchWidth() - 2 * boxDepth() - cardWidth();
//        pLowerPart->setPos(x ,y);
//    }

    if (pUpperPart->collidesWithItem(pLowerPart))
    {
        pUpperPart->setRotation(0);
        x = leftMargin + topSchema->sizeL() + 2 * topSchema->sizeD() + topSchema->sizeH()/2;
        y = topMargin + topSchema->sizeL() + 2 * topSchema->sizeD() + topSchema->sizeW();
        pUpperPart->setPos(x, y);


        pLowerPart->setRotation(0);
        x = leftMargin + bottomSchema->sizeL() + 2 * bottomSchema->sizeD() + bottomSchema->sizeH()/2;
        y = paperHeight + topMargin + bottomSchema->sizeL() + 2 * bottomSchema->sizeD() + bottomSchema->sizeW();
        pLowerPart->setPos(x, y);
        pages = 2;
    }

    for (int p=0; p<pages; p++)
    {
        pScene->addRect(0, p * paperHeight, paperWidth, paperHeight, QPen(Qt::SolidLine));
        pScene->addRect(leftMargin, p * paperHeight + topMargin, pageWidth, pageHeight, QPen(Qt::DotLine));
    }

    QRectF sceneRect = QRectF(0, 0, paperWidth, pages * paperHeight);
    pScene->setSceneRect(sceneRect);
}



void Dialog::onTextureChanged(const QString& textureFilename)
{
    QPixmap pixmap(textureFilename);
    topSchema->setTexture(pixmap);
    bottomSchema->setTexture(pixmap);
}

void Dialog::onFrontImageChanged(const QString& imageFilename)
{
    QPixmap pixmap(imageFilename);
    topSchema->addImage(Schema::Side::F, pixmap, Qt::AlignCenter, false);
}

void Dialog::onLongSideImageChanged(const QString& imageFilename)
{
    QPixmap pixmap(imageFilename);
    topSchema->addImage(Schema::Side::B, pixmap, Qt::AlignLeft | Qt::AlignTop, true);
}

void Dialog::createPoints()
{
    points.insert(0, QPointF(-cardHeight() / 2, boxDepth() + boxDepth() + cardWidth()));
    points.insert(1, QPointF(cardHeight() / 2, boxDepth() + boxDepth() + cardWidth())); // B -- 1
    points.insert(2, QPointF(cardHeight() / 2, boxDepth() + boxDepth() ) ); // C -- 2
    points.insert(3, QPointF(cardHeight() / 2, boxDepth() ) ); // D -- 3
    points.insert(4, QPointF(cardHeight() / 2 + boxDepth(), boxDepth() ) ); // E  - 4
    points.insert(5, QPointF(cardHeight() / 2 + boxDepth(), 0 ) ); //F  - 5
    points.insert(6, QPointF(cardHeight() / 2 + boxDepth() + boxDepth(), 0 ) ); // G  6
    points.insert(7, QPointF(cardHeight() / 2 + boxDepth() + boxDepth() + latchWidth(), -latchWidth() ) ); // H  7
    points.insert(8, QPointF(cardHeight() / 2 + boxDepth() + boxDepth() + latchWidth(), - cardWidth() + latchWidth() ) ); // I  8
    points.insert(9, QPointF(cardHeight() / 2 + boxDepth() + boxDepth(), - cardWidth() ) ); // J  9
    points.insert(10, QPointF(cardHeight() / 2 + boxDepth(), - cardWidth() ) ); // K  10
    points.insert(11, QPointF(cardHeight() / 2 + boxDepth(), - cardWidth()  - boxDepth()) ); // L  11
    points.insert(12, QPointF(cardHeight() / 2 , - cardWidth()  - boxDepth()) ); // M 12
    points.insert(13, QPointF(cardHeight() / 2 , - cardWidth()  - boxDepth() - boxDepth()) ); // N 13
    points.insert(14, QPointF(cardHeight() / 2 - latchWidth(), - cardWidth()  - boxDepth() - boxDepth() - latchWidth() )); // O   14
    points.insert(15, QPointF(-cardHeight() / 2 + latchWidth(), - cardWidth()  - boxDepth() - boxDepth() - latchWidth() )); // P  15
    points.insert(16, QPointF(-cardHeight() / 2 , - cardWidth()  - boxDepth() - boxDepth()) ); // Q   16
    points.insert(17, QPointF(-cardHeight() / 2 , - cardWidth()  - boxDepth()) ); // R   17
    points.insert(18, QPointF(-cardHeight() / 2 - boxDepth(), - cardWidth()  - boxDepth()) ); // S  18
    points.insert(19, QPointF(-cardHeight() / 2 - boxDepth(), - cardWidth()) ); // T  19
    points.insert(20, QPointF(-cardHeight() / 2 - boxDepth() - boxDepth(), - cardWidth()) ); // U   20
    points.insert(21, QPointF(-cardHeight() / 2 - boxDepth() - boxDepth() - latchWidth(), - cardWidth() + latchWidth())); // V  21
    points.insert(22, QPointF(-cardHeight() / 2 - boxDepth() - boxDepth() - latchWidth(), - latchWidth())); // W 22
    points.insert(23, QPointF(-cardHeight() / 2 - boxDepth() - boxDepth() , 0)); // X  23
    points.insert(24, QPointF(-cardHeight() / 2 - boxDepth()  , 0)); // Y  24
    points.insert(25, QPointF(-cardHeight() / 2 - boxDepth()  , boxDepth())); // Z  25
    points.insert(26, QPointF(-cardHeight() / 2  , boxDepth())); // 1  26
    points.insert(27, QPointF(-cardHeight() / 2  , boxDepth() + boxDepth())); // 2  27}

    points.insert(28, QPointF(-cardHeight()/2, 0)); // 28 -- 0
    points.insert(29, QPointF(cardHeight()/2, 0)); // 29 -- 1
    points.insert(30, QPointF(cardHeight()/2, -cardWidth())); // 30 -- 2
    points.insert(31, QPointF(-cardHeight()/2, -cardWidth())); // 31 -- 3
}
