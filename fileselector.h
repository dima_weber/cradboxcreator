#include <QWidget>

class QLineEdit;
class QPushButton;
class QHBoxLayout;

class FileSelector : public QWidget
{
    Q_OBJECT
    QLineEdit* pEdit;
    QPushButton* pOpenFileButton;
    QPushButton* pClearButton;
    QHBoxLayout* pLayout;
    
public:
    FileSelector(QWidget* parent = nullptr);
    QString text() const;

signals:
    void changed(const QString&);
    
private slots:
    void onOpen();
};
