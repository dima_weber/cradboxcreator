#ifndef SCHEMA_H
#define SCHEMA_H

#include <Qt>
#include <QRectF>
#include <QMap>
#include <QPen>

class QGraphicsPathItem;
class QGraphicsPixmapItem;
class QGraphicsTextItem;
class QPixmap;
class QGraphicsLineItem;
class QGraphicsScene;

class Schema
{
    QGraphicsScene* pScene;
    QGraphicsPathItem* pBorder;
    QList<QGraphicsLineItem*> linesList;
    QGraphicsPixmapItem* pTexture;

    double cardWidth;
    double cardHeight;
    double boxDepth;
    double latch;
    double doublePaperDepth;
    bool isTop;
    double dpi;
    const double mm_in_inch = 25.4;

    QMap<int, QPointF> vertex;
public:
    enum Side {F, A, a, B, b, Ai, ai, Bi, bi, Al, al, Bl, bl, E};
    Schema(QGraphicsScene* scene, int cardWidth, int cardHeight, int boxDepth, bool isTop, int dpi);

    Schema& setCardWidth(int width);
    Schema& setCardHeight(int height);
    Schema& setBoxDepth(int depth);

    QGraphicsPathItem* externalBorder();
    QList<QGraphicsLineItem*>& internalLines();

    QGraphicsPixmapItem* setTexture(const QPixmap& texture);
    QGraphicsPixmapItem* addImage(Side side, const QPixmap& pixmap, Qt::Alignment alignment, bool symmetric);
    QGraphicsTextItem*   addText(Side side, const QString& text, Qt::Alignment alignment, bool symmetric);

    QRectF frontRect() const;
    QRectF sideRect(Side side) const;
    QRectF externalSidesRect(Side side) const;

    Side correspondingSide(Side side) const;

    QPointF sideAnchor(Side side) const;
    int sideOrientation(Side side) const;

    QPointF pointOfSymmetry() const;
    QPointF symmetricPoint(const QPointF& point) const;
    QRectF symmetricRect(const QRectF& rect) const;

    QPen externalPen() const;
    QPen internalPen() const;

    double sizeH () const;
    double sizeW () const;
    double sizeD () const;
    double sizeL () const;

    void rebuildVerteces();

};

#endif // SCHEMA_H
