#include "fileselector.h"
#include <QLineEdit>
#include <QPushButton>
#include <QBoxLayout>
#include <QFileDialog>

FileSelector::FileSelector(QWidget* parent)
    :QWidget(parent), 
     pEdit(new QLineEdit(this)),
     pOpenFileButton(new QPushButton(this)),
     pClearButton(new QPushButton(this)),
     pLayout(new QHBoxLayout(this))
{
    pLayout->addWidget(pEdit);
    pLayout->addWidget(pOpenFileButton);
    pLayout->addWidget(pClearButton);
    pLayout->setMargin(0);
    
    pOpenFileButton->setText("...");
    pClearButton->setText("X");

    pEdit->setReadOnly(true);
    
    pOpenFileButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    pOpenFileButton->setMinimumSize(27, 27);
    pOpenFileButton->setMaximumSize(27, 27);

    pClearButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    pClearButton->setMinimumSize(27, 27);
    pClearButton->setMaximumSize(27, 27);

    pEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    connect (pClearButton, SIGNAL(pressed()), pEdit, SLOT(clear()));
    connect (pOpenFileButton, SIGNAL(pressed()), this, SLOT(onOpen()));
    
    connect (pEdit, SIGNAL(textChanged(QString)), this, SIGNAL(changed(QString)));
}

QString FileSelector::text() const
{
    return pEdit->text();
}

void FileSelector::onOpen()
{
    QString filename;
    filename = QFileDialog::getOpenFileName(this, tr("Open File"),
                                            "/home/dkrasnykh/Downloads",
                                            tr("Images (*.png *.xpm *.jpg)"));
    if (!filename.isEmpty())
    {
        pEdit->setText(filename);
    }

}

