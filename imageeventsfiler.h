class QPixmap;
class QGraphicsPixmapItem;

class ImageEventsFilter : public QObject
{
    Q_OBJECT
    QPixmap& pixmap;
    QGraphicsPixmapItem* pItem;
    
public:
    ImageEventsFilter(QPixmap& pixmap, QGraphicsPixmapItem* pItem, QObject* parent = nullptr);
    
    bool eventFilter(QObject *obj, QEvent *event);
};