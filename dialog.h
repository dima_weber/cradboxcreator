#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QGraphicsScene>
#include <QMap>

class QGraphicsPathItem;
class Schema;

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    double cardWidth() const;
    double cardHeight() const;
    double boxDepth() const;
    double latchWidth() const;
    double dpi() const;
    void setDpi(double dpi);

    const double mm_in_inch = 24.5;
public slots:
    void drawOnPrinter();
    void drawSketch();

private slots:
    void onTextureChanged(const QString& textureFilename);
    void onFrontImageChanged(const QString& imageFilename);
    void onLongSideImageChanged(const QString& imageFilename);

private:
    Ui::Dialog *ui;
    QGraphicsScene* pScene;
    double _dpi;
    QMap<int, QPointF> points;

    void createPoints();

    Schema* topSchema;
    Schema* bottomSchema;
    QGraphicsPathItem* pUpperPart;
    QGraphicsPathItem* pLowerPart;

    int pages;

    QGraphicsPathItem* drawSchema();
};

#endif // DIALOG_H
